
//mux1bit4to1: new multiplexor added

module mux_1bit_4to1_assign ( input [3:0] w,                 // 4-bit input w
                         input [3:0] x,                 // input x
                         input [3:0] y,                 // input y
                         input [3:0] z,                 // input z
                         input [1:0] sel,               // sel used to select between inputs
                         output [3:0] out);             // 4-bit output based on input 
 
   assign out = sel[1] ? (sel[0] ? y : z) : (sel[0] ? x : w); 
 
endmodule